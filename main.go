package main

import (
	"fmt"
	"runtime"
)

const (
	u = 2.0
	N = 100000000
)

type work struct {
	start, end int
}

func splitWork(ch chan work, workers int) {
	remN := N
	remW := workers

	for remW > 0 {
		size := remN / remW
		ch <- work{remN - size, remN}
		remN -= size
		remW--
	}
}

func main() {
	workers := runtime.GOMAXPROCS(0)

	passo := (u - 1) / float64(N)

	chw := make(chan work, workers)
	go splitWork(chw, workers)

	res := make(chan float64, workers)
	for i := 0; i < workers; i++ {
		go func() {
			w := <-chw
			soma := 0.0
			for i := w.start; i < w.end; i++ {
				x := 1 + float64(i)*passo
				soma += 0.5 * (1/x + 1/(x+passo))
			}
			res <- soma
		}()
	}

	soma := 0.0
	for i := 0; i < workers; i++ {
		soma += <-res
	}
	soma *= passo

	fmt.Println("ln(2) =", soma)
}
